#!/usr/bin/python

import sys
import datetime
import json
from functools import reduce


def transform(acc, record):
    satellite_id = record.split("|")[1]
    type = record.split("|")[7]

    if satellite_id in acc.keys():
        acc[satellite_id][type].append(record)
    else:
        acc[satellite_id] = {
            "BATT": [record] if type == "BATT" else [],
            "TSTAT": [record] if type == "TSTAT" else []
        }

    return acc


def create_alert(comp, data):
    bucket = {}
    bucket_index = 0

    # Return if array is empty
    if len(data) == 0:
        return []

    start = datetime.datetime.strptime(data[0].split("|")[0], '%Y%m%d %H:%M:%S.%f')
    end = start + datetime.timedelta(minutes=5)
    for val in data:
        split = val.split("|")
        timestamp = datetime.datetime.strptime(split[0], '%Y%m%d %H:%M:%S.%f')
        json_val = {
            "satelliteId": split[1],
            "severity": "RED HIGH",
            "component": split[7],
            "timestamp": timestamp.isoformat()[:-3] + 'Z'
        } if comp == "TSTAT" else {
            "satelliteId": split[1],
            "severity": "RED LOW",
            "component": split[7],
            "timestamp": timestamp.isoformat()[:-3] + 'Z'
        }

        # Start a new 5 minutes and create a new bucket index
        if timestamp > end:
            start = timestamp
            end = start + datetime.timedelta(minutes=5)
            bucket_index += 1

        # For component TSTAT, check if raw value is above red-high-limit: for BATT, check if raw value is below red-low
        if comp == "TSTAT" and float(split[6]) > float(split[2]):
            if bucket_index in bucket.keys():
                bucket[bucket_index].append(json_val)
            else:
                bucket[bucket_index] = [json_val]
        elif comp == "BATT" and float(split[6]) < float(split[5]):
            if bucket_index in bucket.keys():
                bucket[bucket_index].append(json_val)
            else:
                bucket[bucket_index] = [json_val]

    # Get the first alert if there are 3 or more alert in a 5 minute period
    final_alerts = []
    for key in bucket.keys():
        if len(bucket[key]) > 2:
            final_alerts.append(bucket[key][0])

    return final_alerts


def main():
    if len(sys.argv) <= 1:
        sys.exit('Error: No input file given')

    with open(sys.argv[1], "r") as fo:
        string = fo.read().splitlines()

    info = reduce(
        transform,
        string,
        {}
    )

    result = []
    for id in sorted(info.keys()):
        result += create_alert('BATT', info[id]['BATT'])
        result += create_alert('TSTAT', info[id]['TSTAT'])

    print(json.dumps(result, sort_keys=True, indent=4, default=str))


if __name__ == "__main__":
    main()
